# shorturl

#### 介绍
基于netcore3.1和mysql的短链接服务

#### 安装教程

1.  在mysql中创建数据库ShortUrl，执行ShortUrl.sql创建表
2.  在appsettings.json中配置连接字符串、rootUrl、apiKey、AllowedHosts
3.  运行项目

#### 使用说明

1. 生成短链接 {rootUrl}/api?url={url}&exp={seconds}&apikey={apikey}
   其中url为完整链接，exp参数可以省略，返回值：  
   ```{'url':'完整短链接'}``` 或者 ```{'err':'错误信息'}```
   
2. 访问短链接 {rootUrl}/{key} ，也就是生成成功返回的url，key为短链接码

3. 刷新连接记录 {rootUrl}/reflash     
   删除过期的短链接，可以外部定时调用，比如使用[httpquartz](https://gitee.com/loogn/httpquartz)
   

