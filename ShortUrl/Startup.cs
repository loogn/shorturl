using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Web;
using Loogn.OrmLite;
using Loogn.OrmLite.MySql;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace ShortUrl
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            //配置
            Config.ConnStr = Configuration.GetConnectionString("shorturl");
            Config.ApiKeys = new HashSet<string>(Configuration.GetValue<string>("ApiKeys")
                .Split(";", StringSplitOptions.RemoveEmptyEntries));
            Config.RootUrl = Configuration.GetValue<string>("rootUrl");

            OrmLite.RegisterProvider(MySqlCommandDialectProvider.Instance);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            Urls.ReflashUrls();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                //     /api?url={url}&exp={seconds}&apikey={apikey}
                endpoints.MapGet("/api", ApiHandle);
                //     /reflash?apikey=123456
                endpoints.MapGet("/reflash", ReflashHandle);

                endpoints.MapGet("/{key}", AccessHandle);

                endpoints.MapGet("/", async context => { await context.Response.WriteAsync("Hello World!"); });
            });
        }


        private async Task ApiHandle(HttpContext context)
        {
            try
            {
                context.Response.ContentType = "application/json; charset=utf-8";

                Urls model = new Urls();
                if (!context.Request.Query.TryGetValue("url", out var urls))
                {
                    await WriteError(context, "url cannot be empty");
                    return;
                }

                model.Url = HttpUtility.UrlDecode(urls.ToString());

                if (model.Url.Length > 4096)
                {
                    await WriteError(context, "url maxlength is 4096");
                    return;
                }

                if (!context.Request.Query.TryGetValue("apikey", out var apikeys))
                {
                    await WriteError(context, "apikey cannot be empty");
                    return;
                }

                if (!Config.ApiKeys.Contains(apikeys.ToString()))
                {
                    await WriteError(context, "apikey invalid");
                    return;
                }

                var exp = TimeSpan.FromDays(365 * 100); //默认一百年过期

                if (context.Request.Query.TryGetValue("exp", out var exps))
                {
                    if (long.TryParse(exps.ToString(), out long explong))
                    {
                        exp = TimeSpan.FromSeconds(explong);
                    }
                    else
                    {
                        await WriteError(context, "exp为过期秒数");
                        return;
                    }
                }

                model.ExpireTime = DateTime.Now.Add(exp);
                model.Key = Urls.NewKey();
                model.AddTime = DateTime.Now;
                var flag = Urls.AddUrl(model);
                if (flag > 0)
                {
                    Dictionary<string, string> result = new Dictionary<string, string>();
                    result.Add("url", Config.RootUrl + "/" + model.Key);
                    await context.Response.WriteAsync(JsonSerializer.Serialize(result));
                }
                else
                {
                    await WriteError(context, "db error");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                await WriteError(context, "unknown error");
            }
        }

        private async Task ReflashHandle(HttpContext context)
        {
            if (!context.Request.Query.TryGetValue("apikey", out var apikeys))
            {
                await WriteError(context, "apikey cannot be empty");
                return;
            }

            if (!Config.ApiKeys.Contains(apikeys.ToString()))
            {
                await WriteError(context, "apikey invalid");
                return;
            }

            Urls.ReflashUrls();
            await context.Response.WriteAsync("OK");
        }

        private async Task AccessHandle(HttpContext context)
        {
            context.Response.ContentType = "text/plain; charset=utf-8";
            var key = context.GetRouteValue("key")?.ToString();
            var model = Urls.GetUrl(key);
            if (model == null || model.ExpireTime < DateTime.Now)
            {
                await context.Response.WriteAsync("Invalid url", Encoding.UTF8);
                return;
            }

            context.Response.Redirect(model.Url);
        }

        private async Task WriteError(HttpContext context, string err)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            result.Add("err", err);
            await context.Response.WriteAsync(JsonSerializer.Serialize(result));
        }
    }
}