using System.Collections.Generic;

namespace ShortUrl
{
    public class Config
    {
        public static string ConnStr { get; set; }
        public static HashSet<string> ApiKeys { get; set; }
        public static string RootUrl { get; set; }
        
    }
}