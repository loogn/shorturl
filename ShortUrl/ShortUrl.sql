/*
 Navicat Premium Data Transfer

 Source Server         : aliyun
 Source Server Type    : MySQL
 Source Server Version : 80019
 Source Host           : 
 Source Schema         : ShortUrl

 Date: 25/09/2020 10:23:07
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for Urls
-- ----------------------------
DROP TABLE IF EXISTS `Urls`;
CREATE TABLE `Urls` (
  `Id` bigint NOT NULL AUTO_INCREMENT,
  `Key` varchar(10) COLLATE utf8mb4_general_ci NOT NULL COMMENT '键',
  `Url` varchar(4096) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'Url',
  `AddTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ExpireTime` datetime NOT NULL DEFAULT '2099-01-01 00:00:00',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

SET FOREIGN_KEY_CHECKS = 1;
